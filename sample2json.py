#!/usr/bin/env python3


import json
import os
import csv
import re
from os.path import join
import argparse
from collections import defaultdict

parser = argparse.ArgumentParser()
parser.add_argument("--fastq_dir", help="Required. the FULL path to the fastq folder")
parser.add_argument("--meta", help="Required. the FULL path to the tab delimited meta file")
args = parser.parse_args()

assert args.fastq_dir is not None, "please provide the path to the fastq folder"
assert args.meta is not None, "please provide the path to the meta file"


## collect all the fastq.gz full path in to a list
fq_paths = []

for root, dirs, files in os.walk(args.fastq_dir):
    for file in files:
        if file.endswith("fastq.gz"):
            full_path = join(root, file)
            fq_paths.append(full_path)


## start with fastq files
## build a dictionary of dictionary of list
## e.g.
## paired-end
## {"sample1":{"R1": ["normal1_path"],
##            "R2": ["primary1_path"]},
## "sample2":{"R1: ["normal2_path"],
## 			  "R2": ["primary2_path"]}
## }

## single-end
## {"sample1":{"R1": ["normal1_path"]},
## "sample2":{"R1: ["normal2_path"]}
## }


FILES = defaultdict(lambda: defaultdict(list))


with open(args.meta, "r") as f:
    reader = csv.reader(f, delimiter = "\t")
    # skip the header
    header = next(reader)
    for row in reader:
    	sample_name = row[0].strip()
    	fq_name = row[1].strip()
        # R1 or R2
    	reads = row[2].strip()
    	## now just assume the file name in the metafile contained in the fastq file path
    	fq_full_path = [x for x in fq_paths if fq_name in x]
    	if fq_full_path:
            FILES[sample_name][reads].extend(fq_full_path)
    	else:
    		print("sample {sample_name} missing {reads} {fq_name} bam files".format(sample_name = sample_name, reads = reads, fq_name = fq_name))


print()
sample_num = len(FILES.keys())
print ("total {} unique samples will be processed".format(sample_num))
print ("------------------------------------------")
for sample_name in sorted(FILES.keys()):
	for reads in FILES[sample_name]:
		fq_file = "".join(FILES[sample_name][reads])
		print("sample {sample_name}'s {reads} fastq path is {fq_file}".format(sample_name = sample_name, reads = reads, fq_file = fq_file))
print ("------------------------------------------")
print("check the samples.json file for fastqs belong to each sample")

print()

js = json.dumps(FILES, indent = 4, sort_keys=True)
open('samples.json', 'w').writelines(js)
