#! /bin/bash


# invoke Snakemake in cluster mode with custom wrapper scripts

snakemake --rerun-incomplete -j 1000 --local-cores 2 --jobscript ./jobscript.sh \
		  --timestamp --latency-wait 120 \
		  --cluster-config cluster.json --cluster './bsub_cluster.py' \
		  "$@"
