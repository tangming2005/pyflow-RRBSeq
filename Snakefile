
__author__ = "Ming Tang (https://gitlab.com/tangming2005/snakemake_DNAseq_pipeline)"
__license__ = "MIT"

shell.prefix("set -eo pipefail; echo BEGIN at $(date); ")
shell.suffix("; exitstat=$?; echo END at $(date); echo exit status was $exitstat; exit $exitstat")

configfile: "config.yaml"

localrules: all
# localrules will let the rule run locally rather than submitting to cluster
# computing nodes, this is for very small jobs

## fastq or bam path
FILES = json.load(open(config['SAMPLES_JSON']))

CLUSTER = json.load(open(config['CLUSTER_JSON']))

SAMPLES = sorted(FILES.keys())


## list fastq files, for single end now.
if not config["paired_end"]:
    ALL_FASTQ = expand("01seq/{sample}.fastq", sample = SAMPLES)

## list BAM files
ALL_BAM = expand("02aln/{sample}.sorted.bam", sample= SAMPLES)

## QC files
ALL_QC = ["10multiQC/multiQC_log.html"]
ALL_FASTQC  = expand("02fqc/{sample}_fastqc.zip", sample = SAMPLES)
ALL_FLAGSTAT = expand("02aln/{sample}.sorted.bam.flagstat", sample = SAMPLES)
ALL_METHYLDACKEL = expand("03methyldackel/{sample}_CpG.methylKit", sample = SAMPLES)
ALL_BEDGRAPH= expand("04bedgraph/{sample}_CpG.bedGraph", sample = SAMPLES)
TARGETS = []

TARGETS.extend(ALL_BAM)
TARGETS.extend(ALL_FASTQC)
TARGETS.extend(ALL_FLAGSTAT)
TARGETS.extend(ALL_METHYLDACKEL)
TARGETS.extend(ALL_QC)
TARGETS.extend(ALL_BEDGRAPH)


rule all:
    input: TARGETS


# the resulting file names are mysample_tumor.sorted.bam
# mysample_normal.sorted.bam
# wildcard {sample} will be mapped to mysample_tumor or mysample_normal

def get_input_files(wildcards):
    sample_name = wildcards.sample
    if config["from_fastq"]:
        if config["paired_end"]:
            return  FILES[sample_name]['R1'] + FILES[sample_name]['R2']
        ## single end sequencing
        if not config["paired_end"]:
            return FILES[sample_name]['R1']
    ## start with bam files
    if not config["from_fastq"]:
        return FILES[sample_name]


#########################################################################################################################

#                            merge fastqs and trim the adaptors for RRBS                                               #

########################################################################################################################

## now only for single-end
rule merge_fastqs:
    input: get_input_files
    output: temp("01seq/{sample}.fastq")
    log: "00log/{sample}_unzip"
    threads: 1
    params: jobname = "{sample}"
    message: "merging fastqs gunzip -c {input} > {output}"
    shell: "gunzip -c {input} > {output} 2> {log}"

rule fastqc:
    input:  "01seq/{sample}.fastq"
    output: "02fqc/{sample}_fastqc.zip", "02fqc/{sample}_fastqc.html"
    log:    "00log/{sample}_fastqc"
    threads: CLUSTER["fastqc"]["cpu"]
    params : jobname = "{sample}"
    message: "fastqc {input}: {threads}"
    shell:
        """
        module load fastqc
        fastqc -o 02fqc -f fastq --noextract {input} 2> {log}
        """

rule trim_adaptors:
    input: "01seq/{sample}.fastq"
    output: temp("01trimmed_fastqs/{sample}_trimmed.fq")
    threads: 1
    params:
        jobname = "{sample}"
    message: "trimming adaptors for {input}"
    log: "00log/{sample}_trim_adaptors.log"
    shell:
        """
        # cutadapt is python2.x
        source activate root
        trim_galore --rrbs {input} -o 01trimmed_fastqs/ --fastqc
        """



##########################################################################################################################

#                           align fastq files to bam with bwa-meth                                                       #

#                            TO DO: map fastqs per lane with @RG                                                         #

##########################################################################################################################

## align from fastq files
## deal with paired end and single end data
## see https://bitbucket.org/snakemake/snakemake/pull-requests/148/examples-for-paired-read-data-and/diff
## some hints:
## https://bitbucket.org/snakemake/snakemake/issues/37/add-complex-conditional-file-dependency
## conditional rules?
## https://groups.google.com/forum/#!msg/snakemake/qX7RfXDTDe4/cKZBfc_PAAAJ

if config["from_fastq"]:
    rule align:
        input:  "01trimmed_fastqs/{sample}_trimmed.fq"
        output: "02aln/{sample}.sorted.bam", "02aln/{sample}.sorted.bam.bai", "00log/{sample}.align"
        threads: CLUSTER["align"]["cpu"]
        params:
                jobname = "{sample}",
                ## add read group for bwa mem mapping, change accordingly if you know PL:ILLUMINA, LB:library1 PI:200 etc...
                rg = "@RG\\tID:{sample}\\tSM:{sample}",
                custom = config.get("bwmeth_args", "")
        message: "aligning with bwa-meth {input}: {threads} threads"
        log:
            bwa = "00log/{sample}.align",
            markdup = "00log/{sample}.markdup"
        run:
            ## paired end reads
            if config["paired_end"]:
                shell(
                    r"""
                    source activate root
                    python {config[bwmeth_path]} {params.custom} --reference {config[ref_fa]} {input[0]} {input[1]} \
                    --read-group '{params.rg}' 2> {log.bwa} \
                    | samblaster 2> {log.markdup} \
                    | samtools sort -m 2G -@ 5 -T {output[0]}.tmp -o {output[0]}
                    samtools index {output[0]}
                    """)
            # single end  reads
            else:
                shell(
                    r"""
                    source activate root
                    python {config[bwmeth_path]} {params.custom} --reference {config[ref_fa]} {input} \
                    --read-group '{params.rg}' 2> {log.bwa} \
                    | samblaster 2> {log.markdup} \
                    | samtools sort -m 2G -@ 5 -T {output[0]}.tmp -o {output[0]}
                    samtools index {output[0]}
                    """)

##################################################################################################################################

#          if bam files are ready for methylation call extraction, just symbolic link them to 02aln folder                                  #

##################################################################################################################################

## if you have bam files that are ready (went through GATK best practice: bwa mem, indel-realign, recalibrate, BQSR) for mutation calling by mutect, just need to symbolic link the bam files to 01aln folder
## and keep the naming convention.

def symbolic_link(wildcards):
    sample_name = "_".join(wildcards.sample.split("_")[0:-1])
    sample_type = wildcards.sample.split("_")[-1]
    return FILES[sample_name][sample_type]


if not config["from_fastq"]:
    rule symlink_bam:
        input: symbolic_link
        output: "02aln/{sample}.sorted.bam"
        log: "00log/{sample}_symlink.log"
        message: "symbolic linking {wildcards.sample} bam file to 01aln folder"
        threads: 1
        shell:
            """
            ln -s {input} {output} 2> {log}
            """


rule flagstat_bam:
    input:  "02aln/{sample}.sorted.bam"
    output: "02aln/{sample}.sorted.bam.flagstat"
    log:    "00log/{sample}.flagstat_bam"
    threads: 1
    params: jobname = "{sample}"
    message: "flagstat_bam {input}: {threads} threads"
    shell:
        """
        samtools flagstat {input} > {output} 2> {log}
        """

rule multiQC:
    input :
        expand("00log/{sample}.align", sample = SAMPLES),
        expand("02aln/{sample}.sorted.bam.flagstat", sample = SAMPLES),
        expand("02fqc/{sample}_fastqc.zip", sample = SAMPLES)
    output: "10multiQC/multiQC_log.html"
    log: "00log/multiqc.log"
    message: "multiqc for all logs"
    shell:
        """
        multiqc 02fqc 02aln 00log -o 10multiQC -d -f -v -n multiQC_log 2> {log}
        """


####################################################################################################################################

#                       extract methylation calls with MethylDackel                                                              #

###################################################################################################################################


## finish within 30mins for RRBS
## if it is RRBS, one expect to see many duplicates, set --keepDupes to True
## if it is WGBS, do not set --keepDupes. move to config.ymal to set
rule methyldackel:
    input: "02aln/{sample}.sorted.bam", "02aln/{sample}.sorted.bam.bai"
    output: "03methyldackel/{sample}_CpG.methylKit"
    log: "00log/{sample}_methyldackel.log"
    threads: 4
    params:
        jobname = "{sample}",
        custom = config.get("methyldackel_args", "")
    message: "extracting methylation calls with methyldackel for {input}"
    shell:
        """
        source activate root
        MethylDackel extract {config[ref_fa]} {input[0]} {params.custom} -@ 4 --opref {wildcards.sample}
        mv {wildcards.sample}_CpG.methylKit {output}
        """


### make bedgraph file for IGV visulization

rule make_bedgraph:
    input: "02aln/{sample}.sorted.bam", "02aln/{sample}.sorted.bam.bai"
    output: "04bedgraph/{sample}_CpG.bedGraph"
    log: "00log/{sample}_methyldackel_bedgraph.log"
    threads: 4
    params:
        jobname = "{sample}",
        custom = config.get("bedgraph_args", "")
    message: "make methylation bedgraph by methyldackel for {input}"
    shell:
        """
        source activate root
        MethylDackel extract {config[ref_fa]} {input[0]} {params.custom} -@ 4 --opref {wildcards.sample}
        mv {wildcards.sample}_CpG.bedGraph {output}
        """
