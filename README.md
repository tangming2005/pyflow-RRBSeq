## pyflow-RRBSeq
A snakemake pipeline to process RRBS data.

### Citation

### Workflow of the pipeline

```bash
snakemake --forceall --dag | dot -Tpdf > dag.pdf

snakemake --forceall --rulegraph | dot -Tpdf > rulegraph.pdf
snakemake --forceall --rulegraph | dot -Tpng > rulegraph.png
```
### Dependencies

### How to distribute the workflow

### Input files

### output files


### Other computing clusters

If one is using computing clusters other than `LSF`, the customer wrapper `bsub_cluster.py` needs to be re-written.
However, if one does not want a customer wrapper, he can do below:

#### For `Slurm`:

`pyflow-RRBSeq.sh`:

```bash
snakemake --jobs 99999 \
    --timestamp \
    --localcores 4 \
    --latency-wait 240 \
    --cluster-config cluster.json \
    --cluster "sbatch --account {cluster.account} --partition {cluster.partition} --exclusive --nodes 1 --ntasks 24 --time {cluster.time} --job-name {cluster.name} --output {cluster.output} --error {cluster.error}" \
    "$@"
```

Note that each system has a different format for `cluster.json`, for `Slurm`:

```python
{
    "__default__" :
    {
        "account": "mint",
        "time" : "9000",
        "nodes": 1,
        "partition" : "slurm",
        "name" : "{rule}.{wildcards}",
        "output": "{rule}.{wildcards}.out",
        "error": "{rule}.{wildcards}.err"
    },
    "fastqc" :
    {
        "time" : "0:30",
        "partition" : "shared",
        "cpu" : 1
    },
    "methyldackel":
    {
        "time" : "0:30",
        "partition" : "shared",
        "queue": "slurm"
    }
}

```
